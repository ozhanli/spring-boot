package com.ozhanli.repository;

import com.ozhanli.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ibrahim.ozhanli on 28.03.2017.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
