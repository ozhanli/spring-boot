package com.ozhanli.repository;

import com.ozhanli.domain.Card;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by ibrahim.ozhanli on 20.03.2017.
 */
public interface CardRepository extends JpaRepository<Card, Long> {
    Set<Card> findAllByDeckId(Long deckId);
    Page<Card> findAllByDeckId(Long deckId, Pageable pageable);
    void deleteAllByDeckId(Long deckId);
}
