package com.ozhanli.repository;

import com.ozhanli.domain.Deck;
import com.ozhanli.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by ibrahim.ozhanli on 20.03.2017.
 */
public interface DeckRepository extends JpaRepository<Deck, Long> {
    Page<Deck> findAllByUserId(Long userId, Pageable pageable);
}
