package com.ozhanli.web.rest;

import com.ozhanli.domain.Card;
import com.ozhanli.domain.Deck;
import com.ozhanli.service.CardService;
import com.ozhanli.service.DeckService;
import com.ozhanli.web.rest.util.HeaderUtil;
import com.ozhanli.web.rest.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

/**
 * Created by ibrahim.ozhanli on 20.03.2017.
 */
@RestController
public class CardResource {

    private static final String ENTITY_NAME = "card";

    private final CardService cardService;
    private final DeckService deckService;

    @Autowired
    public CardResource(CardService cardService, DeckService deckService) {
        this.cardService = cardService;
        this.deckService = deckService;
    }

    @PostMapping("{deckId}/cards")
    public ResponseEntity<Card> createCard(@PathVariable Long deckId, @RequestBody Card card) throws URISyntaxException {
        Deck foundDeck = deckService.findOne(deckId);
        //
        // Check user has permission to add a card to deck
        //
        // Check deck is exist
        //
        card.setDeck(foundDeck);
        Card response = cardService.save(card);

        return ResponseEntity.created(new URI("/api/" + deckId.toString() + "/cards"))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, "Success"))
                .body(response);
    }

    // TODO: talk dirty to me
    @PutMapping("{deckId}/cards/{cardId}")
    public ResponseEntity<Card> updateCard(
            @PathVariable Long deckId,
            @PathVariable Long cardId,
            @RequestBody Card card) throws  URISyntaxException {
        //
        // Check user has permission to update card
        //
        // Check deck is exist
        //

        // Optional for nullPointerException
        Card foundCard = cardService.findOne(cardId);
        // If card exist
        foundCard.update(card.getFrontContent(), card.getBackContent());
        Card result = cardService.save(foundCard);

        return ResponseEntity.created(new URI("/api/" + deckId.toString() + "/cards"))
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @GetMapping("{deckId}/cards")
    public ResponseEntity<Set<Card>> getAllCardsByDeckId(@PathVariable Long deckId) {
        Set<Card> cards = cardService.findAllByDeckId(deckId);
        HttpHeaders headers = HeaderUtil.createAlert("", deckId.toString() + "/decks");
        return new ResponseEntity<>(cards, headers, HttpStatus.OK);
    }

    @DeleteMapping("{deckId}/cards/{cardId}")
    public void deleteCard(@PathVariable Long cardId) {
        //
        // Check user has permission to delete specified card from deck
        //
        cardService.delete(cardId);
    }
}
