package com.ozhanli.web.rest;

import com.ozhanli.domain.Card;
import com.ozhanli.domain.Deck;
import com.ozhanli.domain.User;
import com.ozhanli.repository.UserRepository;
import com.ozhanli.service.DeckService;
import com.ozhanli.service.UserService;
import com.ozhanli.web.rest.util.HeaderUtil;
import com.ozhanli.web.rest.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Null;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by ibrahim.ozhanli on 20.03.2017.
 */
@RestController
public class DeckResource {

    private static final String ENTITY_NAME = "deck";

    @Autowired
    private DeckService deckService;
    @Autowired
    private UserService userService;

    @PostMapping("{userId}/decks")
    public ResponseEntity<Deck> createDeck(@PathVariable Long userId, @RequestBody Deck deck) throws URISyntaxException {
        User user = userService.findOne(userId);
        if (user.getId() == null) {
            return ResponseEntity.notFound().build();
        }
        deck.setUser(user);
        Deck result = deckService.save(deck);
        return ResponseEntity
                .created(new URI("api/decks" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @PutMapping("/decks/{id}")
    public ResponseEntity<Deck> updateDeck(@PathVariable Long id, @RequestBody Deck deck) throws URISyntaxException {
        Deck existingDeck = deckService.findOne(id);

        existingDeck.setTitle(deck.getTitle());
        existingDeck.setDescription(deck.getDescription());
        existingDeck.setCover(deck.getCover());

        Deck result = deckService.save(existingDeck);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @GetMapping("{userId}/decks")
    public ResponseEntity<List<Deck>> getDecksByUserId(@PathVariable Long userId, Pageable pageable) {
        Page<Deck> page = deckService.findAllByUserId(userId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, userId.toString() + "/decks");
        return new ResponseEntity<List<Deck>>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/decks")
    public ResponseEntity<List<Deck>> getAllDecks(Pageable pageable) {
        Page<Deck> page = deckService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/decks");
        return new ResponseEntity<List<Deck>>(page.getContent(), headers, HttpStatus.OK);
    }

    /*

    @DeleteMapping("/decks/{id}")
    public ResponseEntity<Void> deleteDeck(@PathVariable Long id) {
        Deck deck = deckService.findOne(id);
        if(deck == null) {
            return ResponseEntity.notFound().build();
        }
        deckService.delete(id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
                .build();
    }
    */

}
