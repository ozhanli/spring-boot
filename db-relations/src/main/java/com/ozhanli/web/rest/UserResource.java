package com.ozhanli.web.rest;

import com.ozhanli.domain.Deck;
import com.ozhanli.domain.User;
import com.ozhanli.service.DeckService;
import com.ozhanli.service.UserService;
import com.ozhanli.web.rest.util.HeaderUtil;
import com.ozhanli.web.rest.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by ibrahim.ozhanli on 28.03.2017.
 */
@RestController
public class UserResource {

    private static final String ENTITY_NAME = "user";

    @Autowired
    UserService userService;

    @PostMapping("/users")
    public ResponseEntity<User> createUser(@RequestBody User user) throws URISyntaxException {
        if (user.getId() != null) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexist", "A new user cannot have an ID"))
                    .body(null);
        }

        User result = userService.save(user);
        return ResponseEntity
                .created(new URI("api/users" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers(Pageable pageable) {
        Page<User> page = userService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity getUser(@PathVariable Long id) {
        User user = userService.findOne(id);
        return ResponseEntity.ok().body(user);
    }

}
