package com.ozhanli.service;

import com.ozhanli.domain.Card;
import com.ozhanli.domain.Deck;
import com.ozhanli.repository.CardRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


/**
 * Created by ibrahim.ozhanli on 20.03.2017.
 */
@Service
@Transactional
public class CardService {

    private final Logger log = LoggerFactory.getLogger(CardService.class);

    @Autowired
    private CardRepository cardRepository;

    public Card save(Card card) {
        log.debug("Request to save Card: {}", card);
        Card result = cardRepository.save(card);
        return result;
    }

    @Transactional(readOnly = true)
    public List<Card> findAll() {
        log.debug("Request to get all Cards");
        List<Card> result = cardRepository.findAll();
        return result;
    }

    @Transactional(readOnly = true)
    public Set<Card> findAllByDeckId(Long deckId) {
        return cardRepository.findAllByDeckId(deckId);
    }
    @Transactional(readOnly = true)
    public Page<Card> findAllByDeckId(Long deckId, Pageable pageable) {
        return cardRepository.findAllByDeckId(deckId, pageable);
    }

    @Transactional(readOnly = true)
    public Card findOne(Long id) {
        log.debug("Request to get Card: {}", id);
        return cardRepository.findOne(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Card: {}", id);
        cardRepository.delete(id);
    }

    public void deleteAllByDeckId(Long deckId) {
        log.debug("Request to delete all Cards by Deck Id: {}", deckId);
        cardRepository.deleteAllByDeckId(deckId);
    }

}
