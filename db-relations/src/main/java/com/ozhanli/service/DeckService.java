package com.ozhanli.service;

import com.ozhanli.domain.Card;
import com.ozhanli.domain.Deck;
import com.ozhanli.domain.User;
import com.ozhanli.repository.DeckRepository;
import com.ozhanli.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ibrahim.ozhanli on 20.03.2017.
 */
@Service
public class DeckService {

    Logger log = LoggerFactory.getLogger(DeckService.class);

    @Autowired
    private DeckRepository deckRepository;

    @Autowired
    private UserRepository userRepository;

    public Deck save(Deck deck) {
        log.debug("Request to save Deck: {}", deck);
        Deck result = deckRepository.save(deck);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Deck> findAll(Pageable pageable) {
        log.debug("Request to get all Decks");
        Page<Deck> result = deckRepository.findAll(pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Deck> findAllByUserId(Long userId, Pageable pageable) {
        return deckRepository.findAllByUserId(userId, pageable);
    }

    @Transactional(readOnly = true)
    public Deck findOne(Long id) {
        log.debug("Request to get Deck : {}", id);
        Deck deck = deckRepository.findOne(id);
        return deck;
    }

    public void delete(Long id) {
        log.debug("Request to delete Deck : {}", id);
        deckRepository.delete(id);
    }

}
