package com.ozhanli.service;

import com.ozhanli.domain.User;
import com.ozhanli.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by ibrahim.ozhanli on 28.03.2017.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public User save(User user) {
        log.debug("Request to save User: {}", user);
        return userRepository.save(user);
    }

    @Transactional(readOnly = true)
    public Page<User> findAll(Pageable pageable) {
        log.debug("Request to get all User");
        return userRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public User findOne(Long id) {
        log.debug("Request to get User: {}", id);
        User user = userRepository.findOne(id);
        return user;
    }

    public void delete(Long id) {
        log.debug("Request to delete User: {}", id);
        userRepository.delete(id);
    }

}
