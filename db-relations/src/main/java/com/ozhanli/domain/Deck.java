package com.ozhanli.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by ibrahim.ozhanli on 20.03.2017.
 */
@Entity
public class Deck implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "cover")
    @Lob
    private byte[] cover;

    @OneToMany(mappedBy = "deck")
    private Set<Card> cards = new HashSet<>();

    @ManyToOne
    private User user;

    public Deck() {
    }

    public Deck(String title, String description, byte[] cover, Set<Card> cards, User user) {
        this.title = title;
        this.description = description;
        this.cover = cover;
        this.cards = cards;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getCover() {
        return cover;
    }

    public void setCover(byte[] cover) {
        this.cover = cover;
    }

    public Set<Card> getCards() {
        return cards;
    }

    public void setCards(Set<Card> cards) {
        this.cards = cards;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Deck deck = (Deck) o;
        if (deck.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, deck.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
