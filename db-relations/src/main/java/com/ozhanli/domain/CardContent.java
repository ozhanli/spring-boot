package com.ozhanli.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Lob;

/**
 * Created by ozhanli on 27/03/2017.
 */
@Embeddable
public class CardContent {

    @Column(name = "image")
    @Lob
    private byte[] image;

    @Column(name = "text")
    @Lob
    public String text;

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
