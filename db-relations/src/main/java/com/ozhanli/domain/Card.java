package com.ozhanli.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by ibrahim.ozhanli on 20.03.2017.
 */
@Entity
public class Card implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonIgnore
    private Deck deck;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "text",  column = @Column(name = "front_text")),
            @AttributeOverride(name = "image", column = @Column(name = "front_image"))
    })
    private CardContent frontContent;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "text",  column = @Column(name = "back_text")),
            @AttributeOverride(name = "image", column = @Column(name = "back_image"))
    })
    private CardContent backContent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public CardContent getFrontContent() {
        return frontContent;
    }

    public void setFrontContent(CardContent frontContent) {
        this.frontContent = frontContent;
    }

    public CardContent getBackContent() {
        return backContent;
    }

    public void setBackContent(CardContent backContent) {
        this.backContent = backContent;
    }

    public void update(CardContent frontContent, CardContent backContent) {
        this.deck = deck;
        this.frontContent = frontContent;
        this.backContent = backContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Card card = (Card) o;
        if (card.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, card.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

}
