/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ozhanli.web.rest.vm;
