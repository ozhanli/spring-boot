(function() {
    'use strict';

    angular
        .module('sessionApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
