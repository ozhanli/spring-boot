(function(){
    'use strict';

    angular.module('app', ['ui.router'])

        .run(["$log", function($log) {
            $log.debug('We did it!..')
        }])

        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/");
        }])

})();