(function() {
    'use strict';

    angular
        .module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('main', {
            abstract: true,
            views: {
                'navbar@': {
                    templateUrl: 'app/content/header/header-template.html',
                    controller: 'HeaderController',
                    controllerAs: 'vm'
                },
                'footer@': {
                    templateUrl: 'app/content/footer/footer-template.html',
                    controller: 'FooterController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
