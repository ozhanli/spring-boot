(function() {
    'use strict';

    angular
        .module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('contact', {
            parent: 'main',
            url: '/contact',
            views: {
                'content@': {
                    templateUrl: 'app/content/contact/contact-template.html',
                    controller: 'ContactController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
