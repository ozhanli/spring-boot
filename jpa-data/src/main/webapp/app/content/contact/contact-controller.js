(function() {
    'use strict';

    angular
        .module('app')
        .controller('ContactController', Controller);

    Controller.$inject = ['$log'];

    function Controller ($log) {
        $log.debug("View1 Controller");
    }
})();
