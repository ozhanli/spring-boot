(function() {
    'use strict';

    angular
        .module('app')
        .controller('HeaderController', Controller);

    Controller.$inject = ['$log'];

    function Controller ($log) {
        $log.debug("Header Controller");
    }
})();
