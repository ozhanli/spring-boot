(function() {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', Controller);

    Controller.$inject = ['$log'];

    function Controller ($log) {
        $log.debug("View1 Controller");
    }
})();
