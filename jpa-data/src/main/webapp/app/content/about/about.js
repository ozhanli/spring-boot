(function() {
    'use strict';

    angular
        .module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('about', {
            parent: 'main',
            url: '/about',
            views: {
                'content@': {
                    templateUrl: 'app/content/about/about-template.html',
                    controller: 'AboutController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
