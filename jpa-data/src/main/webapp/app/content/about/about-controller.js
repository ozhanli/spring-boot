(function() {
    'use strict';

    angular
        .module('app')
        .controller('AboutController', Controller);

    Controller.$inject = ['$log'];

    function Controller ($log) {
        $log.debug("View1 Controller");
    }
})();
