(function() {
    'use strict';

    angular
        .module('app')
        .controller('FooterController', Controller);

    Controller.$inject = ['$log'];

    function Controller ($log) {
        $log.debug("Footer Controller");
    }
})();
