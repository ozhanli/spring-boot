package com.tutorial.repository;

import com.tutorial.domain.Course;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by ibrahim.ozhanli on 02.03.2017.
 */
public interface CourseRepository extends CrudRepository<Course, String> {
    // @ManyToOne example
    public List<Course> findByTopicId(String topicId);
}
