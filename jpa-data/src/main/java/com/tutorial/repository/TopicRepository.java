package com.tutorial.repository;

import com.tutorial.domain.Topic;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ozhanli on 01/03/2017.
 */
public interface TopicRepository extends CrudRepository<Topic, String> {

    //getAllTopic
    //getTopic
    //updateTopic
    //deleteTopic

}
