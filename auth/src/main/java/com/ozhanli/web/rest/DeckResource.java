package com.ozhanli.web.rest;

import com.ozhanli.domain.Deck;
import com.ozhanli.service.DeckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by ozhanli on 11/03/2017.
 */
@RestController
@RequestMapping("/api")
public class DeckResource {

    @Autowired
    private DeckService deckService;

    @GetMapping("/decks")
    private List<Deck> getAllDecks() {
        return deckService.getAllDecks();
    }


    /*
    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/users/{id}")
    public User getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @PostMapping(value = "/users")
    public ResponseEntity createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PutMapping(value = "/users/{id}")
    public void updateUser(@RequestBody User user, @PathVariable Long id) {
        userService.updateUser(id, user);
    }

    @DeleteMapping(value = "users/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @GetMapping("/users/{id}/decks")
    public List<Deck> getUserAllDecks(@PathVariable Long id) {
        return userService.getUserAllDecks(id);
    }
    */
}
