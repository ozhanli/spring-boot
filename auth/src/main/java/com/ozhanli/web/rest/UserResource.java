package com.ozhanli.web.rest;

import com.ozhanli.domain.Deck;
import com.ozhanli.domain.User;
import com.ozhanli.service.DeckService;
import com.ozhanli.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozhanli on 07/03/2017.
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    private UserService userService;
    @Autowired
    private DeckService deckService;

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/users/{id}")
    public User getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @PostMapping(value = "/users")
    public ResponseEntity createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PutMapping(value = "/users/{id}")
    public void updateUser(@RequestBody User user, @PathVariable Long id) {
        userService.updateUser(id, user);
    }

    @DeleteMapping(value = "users/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @GetMapping("/users/{id}/decks")
    public List<Deck> getUserAllDecks(@PathVariable Long id) {
        return userService.getUserAllDecks(id);
    }

    @PostMapping("/users/{id}/decks")
    public void createDeck(@RequestBody Deck deck, @PathVariable Long id) {
        User user = userService.getUser(id);
        deck.setUser(user);
        deckService.createDeck(deck);
    }
}
