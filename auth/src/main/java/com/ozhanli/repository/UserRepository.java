package com.ozhanli.repository;

import com.ozhanli.domain.Deck;
import com.ozhanli.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by ozhanli on 07/03/2017.
 */
public interface UserRepository extends JpaRepository<User, Long>{
    Optional<User> findOneByUsername(String username);
    Optional<User> findOneByEmail(String email);
}
