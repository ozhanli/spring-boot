package com.ozhanli.repository;

import com.ozhanli.domain.Deck;
import com.ozhanli.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by ozhanli on 11/03/2017.
 */
public interface DeckRepository extends JpaRepository<Deck, Long> {
    List<Deck> findByUser(User user);
}
