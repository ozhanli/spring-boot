package com.ozhanli.service;

import com.ozhanli.domain.Deck;
import com.ozhanli.repository.DeckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ozhanli on 11/03/2017.
 */
@Service
public class DeckService {

    @Autowired
    private DeckRepository deckRepository;

    public List<Deck> getAllDecks() {
        List<Deck> decks = new ArrayList<>();
        deckRepository.findAll()
                .forEach(decks::add);
        return decks;
    }

    public void createDeck(Deck deck) {
        deckRepository.save(deck);
    }
    /*


    @Autowired
    UserRepository userRepository;
    @Autowired
    DeckRepository deckRepository;

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        userRepository.findAll()
                .forEach(users::add);
        return users;
    }

    public User getUser(Long id) {
        return userRepository.findOne(id);
    }

    public ResponseEntity<User> createUser(User user) {
        if (isUserExists(user))
            return ResponseEntity.badRequest().build();
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    public void updateUser(Long id, User user) {
        User existingUser = userRepository.findOne(id);
        existingUser.setUsername(user.getUsername());
        existingUser.setPassword(user.getEmail());
        existingUser.setEmail(user.getEmail());
        existingUser.setActivated(user.isActivated());
        userRepository.save(existingUser);
    }

    public void deleteUser(Long id) {
        userRepository.delete(id);
    }

    public List<Deck> getUserAllDecks(Long id) {
        User user = userRepository.findOne(id);
        return deckRepository.findByUser(user);
    }

    private boolean isUserExists(User user) {
        Optional<User> existingUser = userRepository.findOneByEmail(user.getEmail());
        if (existingUser.isPresent())
            return true;

        existingUser = userRepository.findOneByUsername(user.getUsername());
        if (existingUser.isPresent())
            return true;

        return false;
    }
     */

}
