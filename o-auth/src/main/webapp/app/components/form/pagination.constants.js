(function() {
    'use strict';

    angular
        .module('oauthApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
