package com.ozhanli.web.rest;

import com.ozhanli.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ozhanli on 09/03/2017.
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    UserService userService;

    @RequestMapping("/users")
    public String getAllUsers() {
        return "All users";
    }
}
