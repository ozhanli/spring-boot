package com.ozhanli.repository;

import com.ozhanli.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by ozhanli on 09/03/2017.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
